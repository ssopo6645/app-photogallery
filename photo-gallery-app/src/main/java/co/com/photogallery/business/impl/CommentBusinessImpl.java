package co.com.photogallery.business.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import co.com.photogallery.business.ICommentBusiness;
import co.com.photogallery.commons.dto.CommentDTO;
import co.com.photogallery.commons.emuns.ResponseEnum;
import co.com.photogallery.commons.emuns.SeverityEnum;
import co.com.photogallery.commons.exception.BusinessException;
import co.com.photogallery.commons.mapper.CommentMapper;
import co.com.photogallery.phaceholder.dto.Post;
import co.com.photogallery.phaceholder.integration.impl.PhaceHolderImpl;

@Component
public class CommentBusinessImpl implements ICommentBusiness {

	@Autowired
	private PhaceHolderImpl phaceHold;

	@Autowired
	private CommentMapper commentMapper;

	/**
	 * Metodo para obtener un album por id
	 * 
	 * @param idAlbum
	 * @return
	 * @throws BusinessException
	 */
	public List<CommentDTO> getCommentByName(String name) throws BusinessException {
		try {
			return phaceHold.getCommentByName(name).stream().map(commentMapper::commentDomainToCommentDto)
					.collect(Collectors.toList());
		} catch (BusinessException e) {
			throw new BusinessException(e.getCode(), e.getMessage(), e.getSeverity());
		}
	}

	/**
	 * Metodo para obtener los comentarios realizados por un usuario
	 * 
	 * @param id
	 * @return
	 * @throws BusinessException
	 */
	public List<CommentDTO> getCommentByUser(int id) throws BusinessException {
		List<CommentDTO> commentList = null;
		try {
			List<Post> posts = phaceHold.getPostsByUser(id);
			if (!posts.isEmpty()) {
				commentList = new ArrayList<CommentDTO>();
				for (Post post : posts) {
					commentList.addAll(phaceHold.getCommentsByPostId(post.getId()).stream()
							.map(commentMapper::commentDomainToCommentDto).collect(Collectors.toList()));
				}
			} else {
				throw new BusinessException(ResponseEnum.ERROR_404.getCode(), ResponseEnum.ERROR_404.getDescription(),
						SeverityEnum.INFO.name());
			}
		} catch (BusinessException e) {
			throw new BusinessException(e.getCode(), e.getMessage(), e.getSeverity());
		}
		return commentList;
	}
}
