package co.com.photogallery.business;

import java.util.List;

import co.com.photogallery.commons.dto.UserDTO;
import co.com.photogallery.commons.exception.BusinessException;

public interface IUserBusiness {

	/**
	 * Metodo para obtener todos los usuarios
	 * 
	 * @return
	 * @throws BusinessException
	 */
	public List<UserDTO> getAllUser() throws BusinessException;

	/**
	 * Metodo para obtener un usuario por Id
	 * 
	 * @param id
	 * @return
	 * @throws BusinessException
	 */
	public UserDTO getUserById(int id) throws BusinessException;

	/**
	 * Metodo para obtener los usuarios sobre un album o permiso especifico
	 * 
	 * @param idPermission
	 * @param idAlbum
	 * @return
	 * @throws BusinessException
	 */
	public List<UserDTO> getUsersByPermissionAlbum(int idPermission, int idAlbum) throws BusinessException;
}
