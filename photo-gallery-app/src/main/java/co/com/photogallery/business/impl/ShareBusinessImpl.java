package co.com.photogallery.business.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import co.com.photogallery.business.IAlbumBusiness;
import co.com.photogallery.business.IShareBusiness;
import co.com.photogallery.business.IUserBusiness;
import co.com.photogallery.commons.Constants;
import co.com.photogallery.commons.dto.ResponseDTO;
import co.com.photogallery.commons.emuns.PermissionEnum;
import co.com.photogallery.commons.emuns.ResponseEnum;
import co.com.photogallery.commons.emuns.SeverityEnum;
import co.com.photogallery.commons.exception.BusinessException;
import co.com.photogallery.model.dao.IAlbumDAO;
import co.com.photogallery.model.dao.IShareDAO;
import co.com.photogallery.model.dao.IUserDAO;

@Component
public class ShareBusinessImpl implements IShareBusiness {

	@Autowired
	private IUserBusiness userBusiness;

	@Autowired
	private IAlbumBusiness albumBusiness;

	@Autowired
	private IShareDAO shareDao;

	@Autowired
	private IUserDAO userDao;

	@Autowired
	private IAlbumDAO albumDao;

	/**
	 * Metodo para compartir un album a un usuario con su respectivo permiso
	 * 
	 * @param idUser
	 * @param idAlbum
	 * @param permission
	 * @return
	 * @throws BusinessException
	 */
	public ResponseDTO shareAlbumForUser(int idUser, int idAlbum, String permission) throws BusinessException {
		ResponseDTO response = null;
		try {
			validateUser(idUser);
			validateAlbum(idAlbum);

			if (shareDao.getShareAlbumAndUser(idUser, idAlbum).isEmpty()) {
				response = new ResponseDTO();
				if (shareDao.insertShareAlbumUser(idUser, idAlbum, getPermission(permission)) == 1) {
					response.setCode(Integer.parseInt(ResponseEnum.CODE_200.getCode()));
					response.setMessage(Constants.TRANSACCION_EXITOSA);
					response.setSeverity(SeverityEnum.INFO.name());
				}
			} else {
				response = new ResponseDTO();
				response.setCode(Integer.parseInt(ResponseEnum.CODE_200.getCode()));
				response.setMessage(Constants.PERMISO_EXISTENTE);
				response.setSeverity(SeverityEnum.INFO.name());
			}
		} catch (BusinessException e) {
			throw new BusinessException(e.getCode(), e.getMessage(), e.getSeverity());
		}
		return response;
	}

	private int getPermission(String permission) {
		int result = 0;
		if (PermissionEnum.fromPermission(permission) != null) {
			result = PermissionEnum.fromPermission(permission).getId();
		}
		return result;
	}

	/**
	 * Metodo encargado de validar si el usuario existe sino lo crea
	 * 
	 * @param idUser
	 * @throws BusinessException
	 */
	private void validateUser(int idUser) throws BusinessException {
		try {
			if (userBusiness.getUserById(idUser) != null && userDao.getUserById(idUser) == 0) {
				userDao.insertUser(idUser);
			}
		} catch (BusinessException e) {
			throw new BusinessException(e.getCode(), e.getMessage(), e.getSeverity());
		}
	}

	/**
	 * Metodo encargado de validar si un album existe sino lo crea
	 * 
	 * @param idAlbum
	 */
	private void validateAlbum(int idAlbum) {
		try {
			if (albumBusiness.getAlbumById(idAlbum) != null && albumDao.getAlbumById(idAlbum) == 0) {
				albumDao.insertAlbum(idAlbum);
			}
		} catch (BusinessException e) {
			albumDao.insertAlbum(idAlbum);
		}
	}

	/**
	 * Metodo para atualizar el permiso sobre un album a un usuario
	 * 
	 * @param idUser
	 * @param idAlbum
	 * @param permission
	 * @return
	 * @throws BusinessException
	 */
	public ResponseDTO updatePermissionShareAlbumUser(int idUser, int idAlbum, String permission)
			throws BusinessException {
		ResponseDTO response = new ResponseDTO();
		try {
			shareDao.updateShareAlbumUser(idUser, idAlbum, PermissionEnum.fromPermission(permission).getId());
			response.setCode(Integer.parseInt(ResponseEnum.CODE_200.getCode()));
			response.setMessage(Constants.TRANSACCION_EXITOSA);
			response.setSeverity(SeverityEnum.INFO.name());
		} catch (BusinessException e) {
			throw new BusinessException(e.getCode(), e.getMessage(), e.getSeverity());
		}
		return response;
	}
}
