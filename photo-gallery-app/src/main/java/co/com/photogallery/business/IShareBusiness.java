package co.com.photogallery.business;

import co.com.photogallery.commons.dto.ResponseDTO;
import co.com.photogallery.commons.exception.BusinessException;

public interface IShareBusiness {

	/**
	 * Metodo para compartir un album a un usuario con su respectivo permiso
	 * 
	 * @param idUser
	 * @param idAlbum
	 * @param permission
	 * @return
	 * @throws BusinessException
	 */
	public ResponseDTO shareAlbumForUser(int idUser, int idAlbum, String permission) throws BusinessException;

	/**
	 * Metodo para atualizar el permiso sobre un album a un usuario
	 * 
	 * @param idUser
	 * @param idAlbum
	 * @param permission
	 * @return
	 * @throws BusinessException
	 */
	public ResponseDTO updatePermissionShareAlbumUser(int idUser, int idAlbum, String permission)
			throws BusinessException;
}
