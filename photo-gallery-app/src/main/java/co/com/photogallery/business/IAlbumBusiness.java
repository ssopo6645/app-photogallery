package co.com.photogallery.business;

import java.util.List;

import co.com.photogallery.commons.dto.AlbumDTO;
import co.com.photogallery.commons.dto.UserDTO;
import co.com.photogallery.commons.exception.BusinessException;

public interface IAlbumBusiness {

	/**
	 * Metodo para obtener todos los albums
	 * 
	 * @return
	 * @throws BusinessException
	 */
	public List<AlbumDTO> getAllAlbums() throws BusinessException;

	/**
	 * Metodo para obtener los albunes de un usuario
	 * 
	 * @param idUser
	 * @return
	 * @throws BusinessException
	 */
	public UserDTO getAlbumsByUser(int idUser) throws BusinessException;

	/**
	 * Metodo para obtener un album por id
	 * 
	 * @param idAlbum
	 * @return
	 * @throws BusinessException
	 */
	public AlbumDTO getAlbumById(int idAlbum) throws BusinessException;
}
