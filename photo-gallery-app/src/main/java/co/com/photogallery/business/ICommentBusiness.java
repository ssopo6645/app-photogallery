package co.com.photogallery.business;

import java.util.List;

import co.com.photogallery.commons.dto.CommentDTO;
import co.com.photogallery.commons.exception.BusinessException;

public interface ICommentBusiness {
	/**
	 * Metodo para obtener un comentario por nombre
	 * 
	 * @param name
	 * @return
	 * @throws BusinessException
	 */
	public List<CommentDTO> getCommentByName(String name) throws BusinessException;

	/**
	 * Metodo para obtener los comentarios realizados por un usuario
	 * 
	 * @param id
	 * @return
	 * @throws BusinessException
	 */
	public List<CommentDTO> getCommentByUser(int id) throws BusinessException;
}
