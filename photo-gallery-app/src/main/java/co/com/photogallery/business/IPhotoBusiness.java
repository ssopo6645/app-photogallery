package co.com.photogallery.business;

import java.util.List;

import co.com.photogallery.commons.dto.PhotoDTO;
import co.com.photogallery.commons.dto.UserDTO;
import co.com.photogallery.commons.exception.BusinessException;

public interface IPhotoBusiness {

	/**
	 * Metodo para obtener todas las fotos
	 * 
	 * @return
	 * @throws BusinessException
	 */
	public List<PhotoDTO> getAllPhotos() throws BusinessException;

	/**
	 * Metodo para obtener las fotos de un usuario
	 * 
	 * @param id
	 * @return
	 * @throws BusinessException
	 */
	public UserDTO getPhotosByUser(int id) throws BusinessException;
}
