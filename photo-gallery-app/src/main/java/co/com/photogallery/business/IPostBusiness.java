package co.com.photogallery.business;

import java.util.List;

import co.com.photogallery.commons.dto.PostDTO;
import co.com.photogallery.commons.exception.BusinessException;

public interface IPostBusiness {

	/**
	 * Metodo para obtener los post realizados por un usuario
	 * 
	 * @param idUser
	 * @return
	 * @throws BusinessException
	 */
	public List<PostDTO> getPostUser(int idUser) throws BusinessException;
}
