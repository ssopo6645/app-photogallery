package co.com.photogallery.business.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import co.com.photogallery.business.IUserBusiness;
import co.com.photogallery.commons.dto.PermissionDTO;
import co.com.photogallery.commons.dto.UserDTO;
import co.com.photogallery.commons.emuns.ResponseEnum;
import co.com.photogallery.commons.emuns.SeverityEnum;
import co.com.photogallery.commons.exception.BusinessException;
import co.com.photogallery.commons.mapper.UserMapper;
import co.com.photogallery.model.dao.IUserDAO;
import co.com.photogallery.phaceholder.integration.impl.PhaceHolderImpl;

@Component
public class UserBusinessImpl implements IUserBusiness {

	@Autowired
	private PhaceHolderImpl phaceHolder;

	@Autowired
	private UserMapper userMapper;

	@Autowired
	private IUserDAO userDao;

	/**
	 * Metodo para obtener todos los usuarios
	 * 
	 * @return
	 * @throws BusinessException
	 */
	public List<UserDTO> getAllUser() throws BusinessException {
		try {
			return phaceHolder.getAllUsers().stream().map(userMapper::userPhaceHolderToUserDto)
					.collect(Collectors.toList());
		} catch (BusinessException e) {
			throw new BusinessException(e.getCode(), e.getMessage(), e.getSeverity());
		}
	}

	/**
	 * Metodo para obtener un usuario por Id
	 * 
	 * @param id
	 * @return
	 * @throws BusinessException
	 */
	public UserDTO getUserById(int id) throws BusinessException {
		try {
			return userMapper.userPhaceHolderToUserDto(phaceHolder.getUserById(id));
		} catch (BusinessException e) {
			throw new BusinessException(e.getCode(), e.getMessage(), e.getSeverity());
		}
	}

	/**
	 * Metodo para obtener los usuarios sobre un album o permiso especifico
	 * 
	 * @param idPermission
	 * @param idAlbum
	 * @return
	 * @throws BusinessException
	 */
	public List<UserDTO> getUsersByPermissionAlbum(int idPermission, int idAlbum) throws BusinessException {
		List<UserDTO> listUser = null;
		try {
			List<PermissionDTO> listPermission = userDao.getUsersByPermissionAlbum(idPermission, idAlbum);
			if (!listPermission.isEmpty()) {
				listUser = new ArrayList<UserDTO>();
				for (PermissionDTO permissionDTO : listPermission) {
					listUser.add(
							userMapper.userPhaceHolderToUserDto(phaceHolder.getUserById(permissionDTO.getIdUser())));
				}
			} else {
				throw new BusinessException(ResponseEnum.ERROR_404.getCode(), ResponseEnum.ERROR_404.getDescription(),
						SeverityEnum.INFO.name());
			}
		} catch (BusinessException e) {
			throw new BusinessException(e.getCode(), e.getMessage(), e.getSeverity());
		}
		return listUser;
	}
}
