package co.com.photogallery.business.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import co.com.photogallery.business.IAlbumBusiness;
import co.com.photogallery.business.IPostBusiness;
import co.com.photogallery.commons.dto.AlbumDTO;
import co.com.photogallery.commons.dto.PermissionDTO;
import co.com.photogallery.commons.dto.UserDTO;
import co.com.photogallery.commons.emuns.ResponseEnum;
import co.com.photogallery.commons.emuns.SeverityEnum;
import co.com.photogallery.commons.exception.BusinessException;
import co.com.photogallery.commons.mapper.AlbumMapper;
import co.com.photogallery.commons.mapper.UserMapper;
import co.com.photogallery.model.dao.IShareDAO;
import co.com.photogallery.phaceholder.dto.User;
import co.com.photogallery.phaceholder.integration.impl.PhaceHolderImpl;

@Component
public class AlbumBusinessImpl implements IAlbumBusiness {

	@Autowired
	private PhaceHolderImpl phaceHolder;

	@Autowired
	private AlbumMapper albumMapper;

	@Autowired
	private UserMapper userMapper;

	@Autowired
	private IPostBusiness postBusiness;

	@Autowired
	private IShareDAO shareDAO;

	/**
	 * Metodo para obtener todos los albums
	 * 
	 * @return
	 * @throws BusinessException
	 */
	public List<AlbumDTO> getAllAlbums() throws BusinessException {
		try {
			return phaceHolder.getAllAlbums().stream().map(albumMapper::albumDomainToAlbumDTO)
					.collect(Collectors.toList());
		} catch (BusinessException e) {
			throw new BusinessException(e.getCode(), e.getMessage(), e.getSeverity());
		}
	}

	/**
	 * Metodo para obtener los albunes de un usuario
	 * 
	 * @param idUser
	 * @return
	 * @throws BusinessException
	 */
	public UserDTO getAlbumsByUser(int idUser) throws BusinessException {
		UserDTO response = new UserDTO();
		try {
			List<AlbumDTO> albumPhace;
			User userPhace = phaceHolder.getUserById(idUser);
			if (userPhace != null) {
				albumPhace = phaceHolder.getAlbumsByUser(idUser).stream().map(albumMapper::albumDomainToAlbumDTO)
						.collect(Collectors.toList());

				response = userMapper.userPhaceHolderToUserDto(userPhace);
				response.setPost(postBusiness.getPostUser(idUser));

				List<PermissionDTO> listPermission = shareDAO.getShareByUserOrAlbum(idUser, 0);
				albumPhace = getShareAlbum(listPermission, albumPhace);
				response.setAlbums(albumPhace);
			} else {
				throw new BusinessException(ResponseEnum.ERROR_404.getCode(), ResponseEnum.ERROR_404.getDescription(),
						SeverityEnum.INFO.name());
			}
		} catch (BusinessException e) {
			throw new BusinessException(e.getCode(), e.getMessage(), e.getSeverity());
		}
		return response;
	}

	/**
	 * Metodo encargado de retornar todos los albunes compartidos a un usuario con
	 * su respectivo permiso
	 * 
	 * @param listPermission
	 * @param albumPhace
	 * @return
	 * @throws BusinessException
	 */
	private List<AlbumDTO> getShareAlbum(List<PermissionDTO> listPermission, List<AlbumDTO> albumPhace)
			throws BusinessException {
		if (!listPermission.isEmpty()) {
			for (PermissionDTO permissionDTO : listPermission) {
				AlbumDTO albumDTO = new AlbumDTO();
				try {
					albumDTO = albumMapper.albumDomainToAlbumDTO(phaceHolder.getAlbumById(permissionDTO.getIdAlbum()));
				} catch (BusinessException e) {
					throw new BusinessException(e.getCode(), e.getMessage(), e.getSeverity());
				}
				albumDTO.setPermission(permissionDTO.getPermission());
				albumPhace.add(albumDTO);
			}
		}
		return albumPhace;
	}

	/**
	 * Metodo para obtener un album por id
	 * 
	 * @param idAlbum
	 * @return
	 * @throws BusinessException
	 */
	public AlbumDTO getAlbumById(int idAlbum) throws BusinessException {
		try {
			return albumMapper.albumDomainToAlbumDTO(phaceHolder.getAlbumById(idAlbum));
		} catch (BusinessException e) {
			throw new BusinessException(e.getCode(), e.getMessage(), e.getSeverity());
		}
	}
}
