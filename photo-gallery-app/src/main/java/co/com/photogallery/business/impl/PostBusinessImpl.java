package co.com.photogallery.business.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import co.com.photogallery.business.IPostBusiness;
import co.com.photogallery.commons.dto.PostDTO;
import co.com.photogallery.commons.exception.BusinessException;
import co.com.photogallery.commons.mapper.PostMapper;
import co.com.photogallery.phaceholder.integration.impl.PhaceHolderImpl;

@Component
public class PostBusinessImpl implements IPostBusiness {

	@Autowired
	private PhaceHolderImpl phaceHolder;

	@Autowired
	private PostMapper postMapper;

	/**
	 * Metodo para obtener los post realizados por un usuario
	 * 
	 * @param idUser
	 * @return
	 * @throws BusinessException
	 */
	public List<PostDTO> getPostUser(int idUser) throws BusinessException {
		try {
			return phaceHolder.getPostsByUser(idUser).stream().map(postMapper::postDomainToPostDto)
					.collect(Collectors.toList());
		} catch (BusinessException e) {
			throw new BusinessException(e.getCode(), e.getMessage(), e.getSeverity());
		}
	}
}
