package co.com.photogallery.business.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import co.com.photogallery.business.IAlbumBusiness;
import co.com.photogallery.business.IPhotoBusiness;
import co.com.photogallery.commons.dto.AlbumDTO;
import co.com.photogallery.commons.dto.PhotoDTO;
import co.com.photogallery.commons.dto.UserDTO;
import co.com.photogallery.commons.emuns.ResponseEnum;
import co.com.photogallery.commons.emuns.SeverityEnum;
import co.com.photogallery.commons.exception.BusinessException;
import co.com.photogallery.commons.mapper.PhotoMapper;
import co.com.photogallery.phaceholder.integration.impl.PhaceHolderImpl;

@Component
public class PhotoBusinessImpl implements IPhotoBusiness {

	@Autowired
	private PhaceHolderImpl phaceHolder;

	@Autowired
	private PhotoMapper photoMapper;

	@Autowired
	private IAlbumBusiness albumBusiness;

	/**
	 * Metodo para obtener todas las fotos
	 * 
	 * @return
	 * @throws BusinessException
	 */
	public List<PhotoDTO> getAllPhotos() throws BusinessException {
		try {
			return phaceHolder.getAllPhotos().stream().map(photoMapper::photoDomainToPhotoDto)
					.collect(Collectors.toList());
		} catch (BusinessException e) {
			throw new BusinessException(e.getCode(), e.getMessage(), e.getSeverity());
		}
	}

	/**
	 * Metodo para obtener las fotos de un usuario
	 * 
	 * @param id
	 * @return
	 * @throws BusinessException
	 */
	public UserDTO getPhotosByUser(int id) throws BusinessException {
		UserDTO user = null;
		try {
			user = albumBusiness.getAlbumsByUser(id);
			if (user != null && !user.getAlbums().isEmpty()) {
				for (AlbumDTO albumDto : user.getAlbums()) {
					albumDto.setPhotos(phaceHolder.getPhotosByUser(albumDto).stream()
							.map(photoMapper::photoDomainToPhotoDto).collect(Collectors.toList()));
				}
			} else {
				throw new BusinessException(ResponseEnum.ERROR_404.getCode(), ResponseEnum.ERROR_404.getDescription(),
						SeverityEnum.INFO.name());
			}
		} catch (BusinessException e) {
			throw new BusinessException(e.getCode(), e.getMessage(), e.getSeverity());
		}
		return user;
	}
}
