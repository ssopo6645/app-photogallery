package co.com.photogallery.commons.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import co.com.photogallery.commons.dto.ResponseDTO;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface ResponseMapper {
	/**
	 * Metodo encargado de convertir a objeto ResponseDTO
	 * 
	 * @param code
	 * @param message
	 * @param severity
	 * @return
	 */
	ResponseDTO responseToDto(String code, String message, String severity);
}
