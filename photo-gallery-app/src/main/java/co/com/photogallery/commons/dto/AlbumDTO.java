package co.com.photogallery.commons.dto;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
public class AlbumDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private @Getter @Setter int id;
	private @Getter @Setter String title;
	private @Getter @Setter String permission;
	private @Getter @Setter List<PhotoDTO> photos;

	@Override
	public String toString() {
		return "AlbumDTO [id=" + id + ", title=" + title + ", permission=" + permission + ", photos=" + photos + "]";
	}
}
