package co.com.photogallery.commons.emuns;

import java.text.MessageFormat;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum ResponseEnum {

	ERROR_502("502", "Error de Comunicación con el sistema legado"), ERROR_400("400", "Dato de entrada Inválido: "),
	ERROR_404("404", "No se encontraron resultados según los parámetros ingresados"),
	ERROR_417("417", "Respuesta Legado: "), ERROR_401("401", "Unauthorized."),
	ERROR_503("503", "Operación no exitosa: "), CODE_200("200", "Transacción Exitosa"),
	ERROR_512("512", "No se lograron cargar las propiedades. ");

	private @Getter String code;
	private String description;

	/**
	 * Metodo que obtiene la descripcion
	 *
	 * @param values
	 * @return
	 */
	public String getDescription(Object... values) {
		if (values.length > 0) {
			return MessageFormat.format(this.description, values);
		} else {
			return description;
		}
	}

	public static ResponseEnum fromCode(String code) {
		for (ResponseEnum b : ResponseEnum.values()) {
			if (b.code.equalsIgnoreCase(code)) {
				System.out.println("return ResponseEnum: " + b);
				return b;
			}
		}
		return null;
	}
}
