package co.com.photogallery.commons.emuns;

public enum PermissionEnum {

	LECTURA(1, "LECTURA"), ESCRITURA(2, "ESCRITURA"), LECTOESCRITURA(3, "LECTURA y ESCRITURA");

	private PermissionEnum(int id, String name) {
		this.id = id;
		this.name = name;
	}

	private int id;
	private String name;

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public static PermissionEnum fromPermission(String code) {
		for (PermissionEnum b : PermissionEnum.values()) {
			if (b.name.equalsIgnoreCase(code)) {
				return b;
			}
		}
		return null;
	}

	public static PermissionEnum fromPermissionById(String code) {
		for (PermissionEnum b : PermissionEnum.values()) {
			if (b.id == Integer.parseInt(code)) {
				return b;
			}
		}
		return null;
	}
}
