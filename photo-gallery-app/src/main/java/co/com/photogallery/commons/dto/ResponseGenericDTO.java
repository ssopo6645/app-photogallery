package co.com.photogallery.commons.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

public class ResponseGenericDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private @Getter @Setter Object payload;

	@Override
	public String toString() {
		return "ResponseDTO [payload=" + payload + "]";
	}
}
