package co.com.photogallery.commons.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import co.com.photogallery.commons.dto.PhotoDTO;
import co.com.photogallery.phaceholder.dto.Photo;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface PhotoMapper {

	/**
	 * Metodo encargado de convertir un objeto Photo a PhotoDTO
	 * 
	 * @param photo
	 * @return
	 */
	PhotoDTO photoDomainToPhotoDto(Photo photo);
}
