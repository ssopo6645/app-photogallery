package co.com.photogallery.commons;

public class Constants {

	public static final String INSERT_ALBUM = "INSERT INTO GALLERYDB.ALBUM (ID_ALBUM) VALUES (?)";
	public static final String GET_ALBUM_BY_ID = "SELECT * FROM GALLERYDB.ALBUM WHERE ID_ALBUM = ?";
	public static final String INSERT_PERMISSION = "INSERT INTO GALLERYDB.PERMISSION (ID_PERMISSION, NAME) VALUES (?,?)";
	public static final String GET_SHARE_ALBUM_USER = "SELECT * FROM GALLERYDB.SHARE WHERE ID_USER = ? and ID_ALBUM = ? and ID_PERMISSION = ?";
	public static final String INSERT_SHARE_ALBUM_USER = "INSERT INTO GALLERYDB.SHARE (ID_ALBUM, ID_PERMISSION, ID_USER) VALUES (?,?,?)";
	public static final String UPDATE_SHARE_ALBUM_USER = "UPDATE GALLERYDB.SHARE SET ID_PERMISSION = ? WHERE ID_ALBUM = ? AND ID_USER = ?";
	public static final String GET_SHARE_ALBUM_AND_USER = "SELECT * FROM GALLERYDB.SHARE WHERE ID_USER = ? and ID_ALBUM = ?";
	public static final String GET_USER_BY_ID = "SELECT * FROM GALLERYDB.USER WHERE ID_USER = ?";
	public static final String INSERT_USER = "INSERT INTO GALLERYDB.USER (ID_USER) VALUES (?)";
	public static final String GET_USER_BY_PERMISSION_ALBUM = "SELECT * FROM GALLERYDB.SHARE WHERE ID_PERMISSION = ? and ID_ALBUM = ?";

	public static final String TRANSACCION_EXITOSA = "Trasaccion Exitosa";
	public static final String PERMISO_EXISTENTE = "Permisos sobre el usuario ya existentes";
}
