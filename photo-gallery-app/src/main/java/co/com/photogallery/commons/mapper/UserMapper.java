package co.com.photogallery.commons.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import co.com.photogallery.commons.dto.UserDTO;
import co.com.photogallery.phaceholder.dto.User;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface UserMapper {

	/**
	 * Metodo encargado de convertir de un objeto User a UserDTO
	 * 
	 * @param user
	 * @return
	 */
	UserDTO userPhaceHolderToUserDto(User user);
}
