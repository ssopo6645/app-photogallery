package co.com.photogallery.commons.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
public class PermissionDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private @Getter @Setter int idAlbum;
	private @Getter @Setter String permission;
	private @Getter @Setter int idUser;

	@Override
	public String toString() {
		return "PermissionRepositoryDTO [idUser=" + idUser + ", idAlbum=" + idAlbum + ", permission=" + permission
				+ "]";
	}
}
