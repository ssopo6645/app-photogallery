package co.com.photogallery.commons.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import co.com.photogallery.commons.dto.AlbumDTO;
import co.com.photogallery.phaceholder.dto.Album;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface AlbumMapper {

	/**
	 * Metodo encargado de convertir un objeto Album a AlbumDTO
	 * 
	 * @param album
	 * @return
	 */
	AlbumDTO albumDomainToAlbumDTO(Album album);
}
