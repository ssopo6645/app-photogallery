package co.com.photogallery.commons.exception;

import lombok.Getter;
import lombok.Setter;

public class BusinessException extends Exception {

	public BusinessException() {
		super();
	}

	public BusinessException(String code, String message, String severity) {
		super();
		this.code = code;
		this.message = message;
		this.severity = severity;
	}

	public BusinessException(String code, String message, String severity, Throwable cause) {
		super();
		this.code = code;
		this.message = message;
		this.severity = severity;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private @Getter @Setter String code;
	private @Getter @Setter String message;
	private @Getter @Setter String severity;
}
