package co.com.photogallery.commons.dto;

import java.io.Serializable;
import java.util.List;

import co.com.photogallery.phaceholder.dto.Address;
import co.com.photogallery.phaceholder.dto.Company;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
public class UserDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private @Getter @Setter String name;
	private @Getter @Setter String username;
	private @Getter @Setter String email;
	private @Getter @Setter Address address;
	private @Getter @Setter String phone;
	private @Getter @Setter String website;
	private @Getter @Setter Company company;
	private @Getter @Setter List<AlbumDTO> albums;
	private @Getter @Setter List<PostDTO> post;

	@Override
	public String toString() {
		return "UserDTO [name=" + name + ", username=" + username + ", email=" + email + ", address=" + address
				+ ", phone=" + phone + ", website=" + website + ", company=" + company + ", albums=" + albums
				+ ", post=" + post + "]";
	}
}
