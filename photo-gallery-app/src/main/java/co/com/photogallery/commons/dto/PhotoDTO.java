package co.com.photogallery.commons.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
public class PhotoDTO implements Serializable {
	/**
	* 
	*/
	private static final long serialVersionUID = 1L;
	private @Getter @Setter int id;
	private @Getter @Setter String title;
	private @Getter @Setter String url;
	private @Getter @Setter String thumbnailUrl;

	@Override
	public String toString() {
		return "PhotoDTO [id=" + id + ", title=" + title + ", url=" + url + ", thumbnailUrl=" + thumbnailUrl + "]";
	}
}
