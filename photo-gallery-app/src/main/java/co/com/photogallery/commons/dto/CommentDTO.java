package co.com.photogallery.commons.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
public class CommentDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private @Getter @Setter String name;
	private @Getter @Setter String email;
	private @Getter @Setter String body;

	@Override
	public String toString() {
		return "CommentDTO [name=" + name + ", email=" + email + ", body=" + body + "]";
	}
}
