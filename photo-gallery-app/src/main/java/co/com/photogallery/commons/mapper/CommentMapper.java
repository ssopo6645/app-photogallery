package co.com.photogallery.commons.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import co.com.photogallery.commons.dto.CommentDTO;
import co.com.photogallery.phaceholder.dto.Comment;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface CommentMapper {

	/**
	 * Metodo encargado de convertir un objeto Comment a CommentDTO
	 * 
	 * @param comment
	 * @return
	 */
	CommentDTO commentDomainToCommentDto(Comment comment);

}
