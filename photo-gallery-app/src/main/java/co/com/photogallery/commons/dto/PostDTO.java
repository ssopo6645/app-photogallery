package co.com.photogallery.commons.dto;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
public class PostDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private @Getter @Setter String title;
	private @Getter @Setter String body;
	private @Getter @Setter List<CommentDTO> comments;

	@Override
	public String toString() {
		return "PostDTO [title=" + title + ", body=" + body + ", comments=" + comments + "]";
	}
}
