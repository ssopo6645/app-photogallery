package co.com.photogallery.commons.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import co.com.photogallery.commons.dto.PostDTO;
import co.com.photogallery.phaceholder.dto.Post;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface PostMapper {

	/**
	 * Metodo encargado de convertir un objeto Post a PostDTO
	 * 
	 * @param post
	 * @return
	 */
	PostDTO postDomainToPostDto(Post post);
}
