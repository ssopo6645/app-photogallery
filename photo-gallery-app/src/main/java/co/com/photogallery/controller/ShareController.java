package co.com.photogallery.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import co.com.photogallery.builder.JSONBuilder;
import co.com.photogallery.business.IShareBusiness;
import co.com.photogallery.commons.dto.PermissionDTO;
import co.com.photogallery.commons.dto.ResponseGenericDTO;
import co.com.photogallery.commons.emuns.ResponseEnum;
import co.com.photogallery.commons.exception.BusinessException;
import co.com.photogallery.commons.mapper.ResponseMapper;

@RestController
@RequestMapping("/photo/gallery/permission")
public class ShareController {

	@Autowired
	private IShareBusiness shareBusiness;

	@Autowired
	private ResponseMapper responseMapper;

	/**
	 * Metodo encargado de compartir un album a un usuario especifico
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/shareAlbum", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<ResponseGenericDTO> shareAlbumForUser(@RequestBody(required = true) PermissionDTO request) {
		ResponseGenericDTO response = new ResponseGenericDTO();
		try {
			response.setPayload(shareBusiness.shareAlbumForUser(request.getIdUser(), request.getIdAlbum(),
					request.getPermission()));
			return ResponseEntity.ok(response);
		} catch (BusinessException e) {
			response.setPayload(responseMapper.responseToDto(e.getCode(), e.getMessage(), e.getSeverity()));
			return ResponseEntity.status(JSONBuilder.toHttpError(ResponseEnum.fromCode(e.getCode()))).body(response);
		}
	}

	/**
	 * Metodo encargado de actualizar el permiso de un usuario sobre un album
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<ResponseGenericDTO> updatePermissionShareAlbumUser(
			@RequestBody(required = true) PermissionDTO request) {
		ResponseGenericDTO response = new ResponseGenericDTO();
		try {
			response.setPayload(shareBusiness.updatePermissionShareAlbumUser(request.getIdUser(), request.getIdAlbum(),
					request.getPermission()));
			return ResponseEntity.ok(response);
		} catch (BusinessException e) {
			response.setPayload(responseMapper.responseToDto(e.getCode(), e.getMessage(), e.getSeverity()));
			return ResponseEntity.status(JSONBuilder.toHttpError(ResponseEnum.fromCode(e.getCode()))).body(response);
		}
	}
}
