package co.com.photogallery.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import co.com.photogallery.builder.JSONBuilder;
import co.com.photogallery.business.IAlbumBusiness;
import co.com.photogallery.commons.dto.ResponseGenericDTO;
import co.com.photogallery.commons.emuns.ResponseEnum;
import co.com.photogallery.commons.exception.BusinessException;
import co.com.photogallery.commons.mapper.ResponseMapper;

@RestController
@RequestMapping("/photo/gallery/albums")
public class AlbumController {

	@Autowired
	private IAlbumBusiness albumBusiness;

	@Autowired
	private ResponseMapper responseMapper;

	/**
	 * 
	 * Metodo encargado de obtener toda la informacion de los albunes
	 * 
	 * @return
	 */
	@RequestMapping(value = "/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<ResponseGenericDTO> getAllAlbums() {
		ResponseGenericDTO response = new ResponseGenericDTO();
		try {
			response.setPayload(albumBusiness.getAllAlbums());
			return ResponseEntity.ok(response);
		} catch (BusinessException e) {
			response.setPayload(responseMapper.responseToDto(e.getCode(), e.getMessage(), e.getSeverity()));
			return ResponseEntity.status(JSONBuilder.toHttpError(ResponseEnum.fromCode(e.getCode()))).body(response);
		}
	}

	/**
	 * 
	 * Metodo encargado de obtener los albunes de un usuario especifico
	 * 
	 * @param userId
	 * @return
	 */
	@RequestMapping(value = "", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<ResponseGenericDTO> getAlbumsByUser(
			@RequestParam(value = "userId", required = true) int userId) {
		ResponseGenericDTO response = new ResponseGenericDTO();
		try {
			response.setPayload(albumBusiness.getAlbumsByUser(userId));
			return ResponseEntity.ok(response);
		} catch (BusinessException e) {
			response.setPayload(responseMapper.responseToDto(e.getCode(), e.getMessage(), e.getSeverity()));
			return ResponseEntity.status(JSONBuilder.toHttpError(ResponseEnum.fromCode(e.getCode()))).body(response);
		}
	}
}
