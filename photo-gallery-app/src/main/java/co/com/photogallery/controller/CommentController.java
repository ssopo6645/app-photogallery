package co.com.photogallery.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import co.com.photogallery.builder.JSONBuilder;
import co.com.photogallery.business.ICommentBusiness;
import co.com.photogallery.commons.dto.ResponseGenericDTO;
import co.com.photogallery.commons.emuns.ResponseEnum;
import co.com.photogallery.commons.exception.BusinessException;
import co.com.photogallery.commons.mapper.ResponseMapper;

@RestController
@RequestMapping("/photo/gallery/comments")
public class CommentController {

	@Autowired
	private ICommentBusiness commentBusiness;

	@Autowired
	private ResponseMapper responseMapper;

	/**
	 * Metodo de obtener un comentario por nombre
	 * 
	 * @param name
	 * @return
	 */
	@RequestMapping(value = "", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<ResponseGenericDTO> getCommentByName(
			@RequestParam(value = "name", required = true) String name) {
		ResponseGenericDTO response = new ResponseGenericDTO();
		try {
			response.setPayload(commentBusiness.getCommentByName(name));
			return ResponseEntity.ok(response);
		} catch (BusinessException e) {
			response.setPayload(responseMapper.responseToDto(e.getCode(), e.getMessage(), e.getSeverity()));
			return ResponseEntity.status(JSONBuilder.toHttpError(ResponseEnum.fromCode(e.getCode()))).body(response);
		}
	}

	/**
	 * Metodo encargado de obtener los comentarios realizados por un usuario
	 * especifico
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/user/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<ResponseGenericDTO> getCommentsByUser(@PathVariable(value = "id", required = true) int id) {
		ResponseGenericDTO response = new ResponseGenericDTO();
		try {
			response.setPayload(commentBusiness.getCommentByUser(id));
			return ResponseEntity.ok(response);
		} catch (BusinessException e) {
			response.setPayload(responseMapper.responseToDto(e.getCode(), e.getMessage(), e.getSeverity()));
			return ResponseEntity.status(JSONBuilder.toHttpError(ResponseEnum.fromCode(e.getCode()))).body(response);
		}
	}
}
