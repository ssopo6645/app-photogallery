package co.com.photogallery.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import co.com.photogallery.builder.JSONBuilder;
import co.com.photogallery.business.IPhotoBusiness;
import co.com.photogallery.commons.dto.ResponseGenericDTO;
import co.com.photogallery.commons.emuns.ResponseEnum;
import co.com.photogallery.commons.exception.BusinessException;
import co.com.photogallery.commons.mapper.ResponseMapper;

@RestController
@RequestMapping("/photo/gallery/photos")
public class PhotoController {

	@Autowired
	private IPhotoBusiness photoBusiness;

	@Autowired
	private ResponseMapper responseMapper;

	/**
	 * Metodo encargado de obtener todas las fotos
	 * 
	 * @return
	 */
	@RequestMapping(value = "/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseGenericDTO> getAllPhotos() {
		ResponseGenericDTO response = new ResponseGenericDTO();
		try {
			response.setPayload(photoBusiness.getAllPhotos());
			return ResponseEntity.ok(response);
		} catch (BusinessException e) {
			response.setPayload(responseMapper.responseToDto(e.getCode(), e.getMessage(), e.getSeverity()));
			return ResponseEntity.status(JSONBuilder.toHttpError(ResponseEnum.fromCode(e.getCode()))).body(response);
		}
	}

	/**
	 * Metodo encargado de obtener las fotos de un usuario especifico
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/user/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseGenericDTO> getPhotosByUser(@PathVariable(value = "id") int id) {
		ResponseGenericDTO response = new ResponseGenericDTO();
		try {
			response.setPayload(photoBusiness.getPhotosByUser(id));
			return ResponseEntity.ok(response);
		} catch (BusinessException e) {
			response.setPayload(responseMapper.responseToDto(e.getCode(), e.getMessage(), e.getSeverity()));
			return ResponseEntity.status(JSONBuilder.toHttpError(ResponseEnum.fromCode(e.getCode()))).body(response);
		}
	}
}
