package co.com.photogallery.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import co.com.photogallery.builder.JSONBuilder;
import co.com.photogallery.business.IUserBusiness;
import co.com.photogallery.commons.dto.ResponseGenericDTO;
import co.com.photogallery.commons.emuns.PermissionEnum;
import co.com.photogallery.commons.emuns.ResponseEnum;
import co.com.photogallery.commons.exception.BusinessException;
import co.com.photogallery.commons.mapper.ResponseMapper;

@RestController
@RequestMapping("/photo/gallery/users")
public class UserController {

	@Autowired
	private IUserBusiness userBusiness;

	@Autowired
	private ResponseMapper responseMapper;

	/**
	 * Metodo encargado de retornar toda la informacion de los usuarios
	 * 
	 * @return
	 */
	@RequestMapping(value = "/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<ResponseGenericDTO> getAllUser() {
		ResponseGenericDTO response = new ResponseGenericDTO();
		try {
			response.setPayload(userBusiness.getAllUser());
			return ResponseEntity.ok(response);
		} catch (BusinessException e) {
			response.setPayload(responseMapper.responseToDto(e.getCode(), e.getMessage(), e.getSeverity()));
			return ResponseEntity.status(JSONBuilder.toHttpError(ResponseEnum.fromCode(e.getCode()))).body(response);
		}
	}

	/**
	 * Metodo encargado de obtener los usuarios por medio del album o permiso
	 * especifico
	 * 
	 * @param permission
	 * @param idAlbum
	 * @return
	 */
	@RequestMapping(value = "/album", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<ResponseGenericDTO> getUserByPermissionAlbum(
			@RequestParam(value = "permission") String permission, @RequestParam(value = "idAlbum") int idAlbum) {
		ResponseGenericDTO response = new ResponseGenericDTO();
		try {
			response.setPayload(
					userBusiness.getUsersByPermissionAlbum(PermissionEnum.fromPermission(permission).getId(), idAlbum));
			return ResponseEntity.ok(response);
		} catch (BusinessException e) {
			response.setPayload(responseMapper.responseToDto(e.getCode(), e.getMessage(), e.getSeverity()));
			return ResponseEntity.status(JSONBuilder.toHttpError(ResponseEnum.fromCode(e.getCode()))).body(response);
		}
	}
}
