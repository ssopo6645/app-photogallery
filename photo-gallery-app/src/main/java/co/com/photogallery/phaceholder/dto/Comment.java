package co.com.photogallery.phaceholder.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
public class Comment implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private @Getter @Setter int postId;
	private @Getter @Setter int id;
	private @Getter @Setter String name;
	private @Getter @Setter String email;
	private @Getter @Setter String body;

	@Override
	public String toString() {
		return "Comment [postId=" + postId + ", id=" + id + ", name=" + name + ", email=" + email + ", body=" + body
				+ "]";
	}
}
