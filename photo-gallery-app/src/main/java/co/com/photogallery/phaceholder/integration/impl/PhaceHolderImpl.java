package co.com.photogallery.phaceholder.integration.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import co.com.photogallery.commons.dto.AlbumDTO;
import co.com.photogallery.commons.emuns.ResponseEnum;
import co.com.photogallery.commons.emuns.SeverityEnum;
import co.com.photogallery.commons.exception.BusinessException;
import co.com.photogallery.phaceholder.dto.Album;
import co.com.photogallery.phaceholder.dto.Comment;
import co.com.photogallery.phaceholder.dto.Photo;
import co.com.photogallery.phaceholder.dto.Post;
import co.com.photogallery.phaceholder.dto.User;
import co.com.photogallery.phaceholder.integration.IPhaceHolder;
import feign.FeignException;
import feign.RetryableException;

@Component
public class PhaceHolderImpl {

	@Autowired
	private IPhaceHolder phaceHolder;

	/**
	 * Metodo que retorna todos los usuarios
	 * 
	 * @return
	 * @throws BusinessException
	 */
	public List<User> getAllUsers() throws BusinessException {
		try {
			return phaceHolder.getAllUser();
		} catch (RetryableException e) {
			throw new BusinessException(ResponseEnum.ERROR_502.getCode(), ResponseEnum.ERROR_502.getDescription(),
					SeverityEnum.ERROR.name());
		} catch (FeignException e) {
			throw new BusinessException(ResponseEnum.ERROR_404.getCode(), ResponseEnum.ERROR_404.getDescription(),
					SeverityEnum.INFO.name());
		}
	}

	/**
	 * Metodo que obtiene todos los albunes
	 * 
	 * @return
	 * @throws BusinessException
	 */
	public List<Album> getAllAlbums() throws BusinessException {
		try {
			return phaceHolder.getAllAlbum();
		} catch (RetryableException e) {
			throw new BusinessException(ResponseEnum.ERROR_502.getCode(), ResponseEnum.ERROR_502.getDescription(),
					SeverityEnum.ERROR.name());
		} catch (FeignException e) {
			throw new BusinessException(ResponseEnum.ERROR_404.getCode(), ResponseEnum.ERROR_404.getDescription(),
					SeverityEnum.INFO.name());
		}
	}

	/**
	 * Metodo que obtiene todas las fotos
	 * 
	 * @return
	 * @throws BusinessException
	 */
	public List<Photo> getAllPhotos() throws BusinessException {
		try {
			return phaceHolder.getAllPhoto();
		} catch (RetryableException e) {
			throw new BusinessException(ResponseEnum.ERROR_502.getCode(), ResponseEnum.ERROR_502.getDescription(),
					SeverityEnum.ERROR.name());
		} catch (FeignException e) {
			throw new BusinessException(ResponseEnum.ERROR_404.getCode(), ResponseEnum.ERROR_404.getDescription(),
					SeverityEnum.INFO.name());
		}
	}

	/**
	 * Metodo que obtiene la lista de albunes por un usuario especifico
	 * 
	 * @param idUser
	 * @return
	 * @throws BusinessException
	 */
	public List<Album> getAlbumsByUser(int idUser) throws BusinessException {
		try {
			return phaceHolder.getAlbumsByUser(idUser);
		} catch (RetryableException e) {
			throw new BusinessException(ResponseEnum.ERROR_502.getCode(), ResponseEnum.ERROR_502.getDescription(),
					SeverityEnum.ERROR.name());
		} catch (FeignException e) {
			throw new BusinessException(ResponseEnum.ERROR_404.getCode(), ResponseEnum.ERROR_404.getDescription(),
					SeverityEnum.INFO.name());
		}
	}

	/**
	 * Metodo que obtiene la lista de fotos por album
	 * 
	 * @param listAlbum
	 * @return
	 * @throws BusinessException
	 */
	public List<Photo> getPhotosByUser(AlbumDTO listAlbum) throws BusinessException {
		List<Photo> listPhotos;
		try {
			if (listAlbum != null) {
				listPhotos = new ArrayList<Photo>();
				listPhotos.addAll(phaceHolder.getPhotosByAlbumId(listAlbum.getId()));
			} else {
				throw new BusinessException(ResponseEnum.ERROR_404.getCode(), ResponseEnum.ERROR_404.getDescription(),
						SeverityEnum.INFO.name());
			}
		} catch (RetryableException e) {
			throw new BusinessException(ResponseEnum.ERROR_502.getCode(), ResponseEnum.ERROR_502.getDescription(),
					SeverityEnum.ERROR.name());
		} catch (FeignException e) {
			throw new BusinessException(ResponseEnum.ERROR_404.getCode(), ResponseEnum.ERROR_404.getDescription(),
					SeverityEnum.INFO.name());
		}
		return listPhotos;
	}

	/**
	 * Metodo que obtiene un usuario especifico por id
	 * 
	 * @param idUser
	 * @return
	 * @throws BusinessException
	 */
	public User getUserById(int idUser) throws BusinessException {
		try {
			return phaceHolder.getUserById(idUser);
		} catch (RetryableException e) {
			throw new BusinessException(ResponseEnum.ERROR_502.getCode(), ResponseEnum.ERROR_502.getDescription(),
					SeverityEnum.ERROR.name());
		}
	}

	/**
	 * Metodo que obtiene un album por id
	 * 
	 * @param idAlbum
	 * @return
	 * @throws BusinessException
	 */
	public Album getAlbumById(int idAlbum) throws BusinessException {
		try {
			return phaceHolder.getAlbumById(idAlbum);
		} catch (RetryableException e) {
			throw new BusinessException(ResponseEnum.ERROR_502.getCode(), ResponseEnum.ERROR_502.getDescription(),
					SeverityEnum.ERROR.name());
		}
	}

	/**
	 * Metodo que obtiene la lista de posts realizados por un usuario
	 * 
	 * @param idUser
	 * @return
	 * @throws BusinessException
	 */
	public List<Post> getPostsByUser(int idUser) throws BusinessException {
		try {
			return phaceHolder.getPostsByUser(idUser);
		} catch (RetryableException e) {
			throw new BusinessException(ResponseEnum.ERROR_502.getCode(), ResponseEnum.ERROR_502.getDescription(),
					SeverityEnum.ERROR.name());
		}
	}

	/**
	 * Metodo que obtiene la lista de comentarios por un post
	 * 
	 * @param idPost
	 * @return
	 * @throws BusinessException
	 */
	public List<Comment> getCommentsByPostId(int idPost) throws BusinessException {
		try {
			return phaceHolder.getCommentsByPostId(idPost);
		} catch (RetryableException e) {
			throw new BusinessException(ResponseEnum.ERROR_502.getCode(), ResponseEnum.ERROR_502.getDescription(),
					SeverityEnum.ERROR.name());
		}
	}

	/**
	 * Metodo que obtiene los comentarios por name
	 * 
	 * @param name
	 * @return
	 * @throws BusinessException
	 */
	public List<Comment> getCommentByName(String name) throws BusinessException {
		try {
			return phaceHolder.getCommentsByName(name);
		} catch (RetryableException e) {
			throw new BusinessException(ResponseEnum.ERROR_502.getCode(), ResponseEnum.ERROR_502.getDescription(),
					SeverityEnum.ERROR.name());
		} catch (FeignException e) {
			throw new BusinessException(ResponseEnum.ERROR_404.getCode(), ResponseEnum.ERROR_404.getDescription(),
					SeverityEnum.INFO.name());
		}
	}
}
