package co.com.photogallery.phaceholder.integration;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import co.com.photogallery.phaceholder.dto.Album;
import co.com.photogallery.phaceholder.dto.Comment;
import co.com.photogallery.phaceholder.dto.Photo;
import co.com.photogallery.phaceholder.dto.Post;
import co.com.photogallery.phaceholder.dto.User;

@FeignClient(name = "${feign.placeholder.name}", url = "${feign.placeholder.rootUrl}")
public interface IPhaceHolder {

	/**
	 * Metodo que obtiene todos los usuarios
	 * 
	 * @return
	 */
	@RequestMapping(value = "${feign.placeholder.resources.users}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<User> getAllUser();

	/**
	 * Metodo que obtiene todas las fotos
	 * 
	 * @return
	 */
	@RequestMapping(value = "${feign.placeholder.resources.photos}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Photo> getAllPhoto();

	/**
	 * Metodo que obtiene todos los albunes
	 * 
	 * @return
	 */
	@RequestMapping(value = "${feign.placeholder.resources.albums}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Album> getAllAlbum();

	/**
	 * Metodo que obtiene todos los albunes de un usuario
	 * 
	 * @param userId
	 * @return
	 */
	@RequestMapping(value = "${feign.placeholder.resources.albums}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Album> getAlbumsByUser(@RequestParam(value = "userId", required = true) int userId);

	/**
	 * Metodo que obtiene los comentarios por name
	 * 
	 * @param name
	 * @return
	 */
	@RequestMapping(value = "${feign.placeholder.resources.comments}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Comment> getCommentsByName(@RequestParam(value = "name", required = true) String name);

	/**
	 * Metodo que obtiene todos los post de un usuario
	 * 
	 * @param userId
	 * @return
	 */
	@RequestMapping(value = "${feign.placeholder.resources.posts}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Post> getPostsByUser(@RequestParam(value = "userId", required = true) int userId);

	/**
	 * Metodo que obtiene un usuario por id
	 * 
	 * @param idUser
	 * @return
	 */
	@RequestMapping(value = "${feign.placeholder.resources.users}/{idUser}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public User getUserById(@PathVariable("idUser") int idUser);

	/**
	 * Metodo que obtiene un album por id
	 * 
	 * @param idAlbum
	 * @return
	 */
	@RequestMapping(value = "${feign.placeholder.resources.albums}/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Album getAlbumById(@PathVariable(value = "id", required = true) int idAlbum);

	/**
	 * Metodo que obtiene todos los comentarios de un post
	 * 
	 * @param postId
	 * @return
	 */
	@RequestMapping(value = "${feign.placeholder.resources.comments}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Comment> getCommentsByPostId(@RequestParam(value = "postId", required = true) int postId);

	/**
	 * Metodo que obtiene un post por id
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "${feign.placeholder.resources.posts}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Post getPostsById(@RequestParam(value = "id", required = true) int id);

	/**
	 * Metodo que obtiene todas las fotos de un album
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "${feign.placeholder.resources.photos}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Photo> getPhotosByAlbumId(@RequestParam(value = "albumId", required = true) int id);
}
