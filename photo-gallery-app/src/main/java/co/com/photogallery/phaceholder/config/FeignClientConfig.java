package co.com.photogallery.phaceholder.config;

import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableFeignClients("co.com.photogallery.phaceholder.integration")
public class FeignClientConfig {

}
