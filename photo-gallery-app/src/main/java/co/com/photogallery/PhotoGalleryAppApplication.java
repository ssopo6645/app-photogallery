package co.com.photogallery;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import co.com.photogallery.commons.emuns.PermissionEnum;
import co.com.photogallery.model.dao.IPermissionDAO;

@SpringBootApplication
public class PhotoGalleryAppApplication {

	@Autowired
	private IPermissionDAO permission;

	@PostConstruct
	public void init() {
		permission.insertPermission(PermissionEnum.ESCRITURA.getId(), PermissionEnum.ESCRITURA.getName());
		permission.insertPermission(PermissionEnum.LECTURA.getId(), PermissionEnum.LECTURA.getName());
		permission.insertPermission(PermissionEnum.LECTOESCRITURA.getId(), PermissionEnum.LECTOESCRITURA.getName());
	}

	public static void main(String[] args) {
		SpringApplication.run(PhotoGalleryAppApplication.class, args);
	}

}
