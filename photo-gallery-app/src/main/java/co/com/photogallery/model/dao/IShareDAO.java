package co.com.photogallery.model.dao;

import java.util.List;

import co.com.photogallery.commons.dto.PermissionDTO;
import co.com.photogallery.commons.exception.BusinessException;

public interface IShareDAO {

	/**
	 * Metodo encargado de buscar si un album y un usuario tienen permisos asigados
	 * 
	 * @param idUser
	 * @param idAlbum
	 * @return
	 */
	public List<PermissionDTO> getShareAlbumAndUser(int idUser, int idAlbum);

	/**
	 * Metodo encargado de insertar un permiso de un album sobre un usuario
	 * 
	 * @param idUser
	 * @param idAlbum
	 * @param permission
	 * @return
	 * @throws BusinessException
	 */
	public int insertShareAlbumUser(int idUser, int idAlbum, int permission) throws BusinessException;

	/**
	 * Metodo encargado de buscar si existe permisos sobre un usuario o un album
	 * 
	 * @param idUser
	 * @param idAlbum
	 * @return
	 * @throws BusinessException
	 */
	public List<PermissionDTO> getShareByUserOrAlbum(int idUser, int idAlbum) throws BusinessException;

	/**
	 * Metodo encargado de actualizar un permiso sobre un album par aun usuario
	 * 
	 * @param idUser
	 * @param idAlbum
	 * @param permission
	 * @return
	 * @throws BusinessException
	 */
	public int updateShareAlbumUser(int idUser, int idAlbum, int permission) throws BusinessException;
}
