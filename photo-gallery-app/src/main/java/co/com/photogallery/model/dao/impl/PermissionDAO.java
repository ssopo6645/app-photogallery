package co.com.photogallery.model.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import co.com.photogallery.commons.Constants;
import co.com.photogallery.model.dao.IPermissionDAO;

@Service
public class PermissionDAO implements IPermissionDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	/**
	 * Metodo encargado de insertar los permisos a la base de datos
	 * 
	 * @param id
	 * @param name
	 * @return
	 */
	@Override
	public int insertPermission(int id, String name) {
		return jdbcTemplate.update(Constants.INSERT_PERMISSION, new Object[] { id, name });
	}
}
