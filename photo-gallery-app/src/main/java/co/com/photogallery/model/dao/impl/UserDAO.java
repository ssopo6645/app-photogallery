package co.com.photogallery.model.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import co.com.photogallery.commons.Constants;
import co.com.photogallery.commons.dto.PermissionDTO;
import co.com.photogallery.commons.emuns.PermissionEnum;
import co.com.photogallery.model.dao.IUserDAO;

@Service
public class UserDAO implements IUserDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	/**
	 * Metodo encargado de consultar un usuario por id en base de datos
	 * 
	 * @param id
	 * @return
	 */
	public int getUserById(int id) {
		int result;
		try {
			result = jdbcTemplate.queryForObject(Constants.GET_USER_BY_ID, new Object[] { id }, Integer.class);
		} catch (EmptyResultDataAccessException e) {
			result = 0;
		}
		return result;
	}

	/**
	 * Metodo encargado de insertar un usuario a la base de datos
	 * 
	 * @param id
	 * @return
	 */
	public int insertUser(int id) {
		return jdbcTemplate.update(Constants.INSERT_USER, new Object[] { id });
	}

	/**
	 * Metodo encargado de retornar los usuarios por permiso y album especifico
	 * 
	 * @param idPermission
	 * @param idAlbum
	 * @return
	 */
	public List<PermissionDTO> getUsersByPermissionAlbum(int idPermission, int idAlbum) {
		List<PermissionDTO> result;
		try {
			result = jdbcTemplate.query(Constants.GET_USER_BY_PERMISSION_ALBUM, new Object[] { idPermission, idAlbum },
					new RowMapper<PermissionDTO>() {
						public PermissionDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
							PermissionDTO c = new PermissionDTO();
							c.setIdAlbum(rs.getInt(1));
							c.setPermission(PermissionEnum.fromPermissionById(rs.getString(2)).getName());
							c.setIdUser(rs.getInt(3));
							return c;
						}
					});
		} catch (EmptyResultDataAccessException e) {
			result = null;
		}
		return result;
	}
}
