package co.com.photogallery.model.dao;

public interface IAlbumDAO {

	/**
	 * Metodo encargado de insertar un album a la base de datos
	 * 
	 * @param idAlbum
	 * @return
	 */
	public int insertAlbum(int idAlbum);

	/**
	 * Metodo encargado de buscar un album en la base de datos
	 * 
	 * @param idAlbum
	 * @return
	 */
	public int getAlbumById(int idAlbum);
}
