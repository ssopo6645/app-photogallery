package co.com.photogallery.model.dao;

import java.util.List;

import co.com.photogallery.commons.dto.PermissionDTO;

public interface IUserDAO {

	/**
	 * Metodo encargado de consultar un usuario por id en base de datos
	 * 
	 * @param id
	 * @return
	 */
	public int getUserById(int id);

	/**
	 * Metodo encargado de insertar un usuario a la base de datos
	 * 
	 * @param id
	 * @return
	 */
	public int insertUser(int id);

	/**
	 * Metodo encargado de retornar los usuarios por permiso y album especifico
	 * 
	 * @param idPermission
	 * @param idAlbum
	 * @return
	 */
	public List<PermissionDTO> getUsersByPermissionAlbum(int idPermission, int idAlbum);
}
