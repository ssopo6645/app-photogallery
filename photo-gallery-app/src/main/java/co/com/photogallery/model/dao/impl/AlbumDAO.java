package co.com.photogallery.model.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import co.com.photogallery.commons.Constants;
import co.com.photogallery.model.dao.IAlbumDAO;

@Service
public class AlbumDAO implements IAlbumDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	/**
	 * Metodo encargado de insertar un album a la base de datos
	 * 
	 * @param idAlbum
	 * @return
	 */
	@Override
	public int insertAlbum(int idAlbum) {
		return jdbcTemplate.update(Constants.INSERT_ALBUM, new Object[] { idAlbum });
	}

	/**
	 * Metodo encargado de buscar un album en la base de datos
	 * 
	 * @param idAlbum
	 * @return
	 */
	@Override
	public int getAlbumById(int idAlbum) {
		int result;
		try {
			result = jdbcTemplate.queryForObject(Constants.GET_ALBUM_BY_ID, new Object[] { idAlbum }, Integer.class);
		} catch (EmptyResultDataAccessException e) {
			result = 0;
		}
		return result;
	}
}
