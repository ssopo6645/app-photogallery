package co.com.photogallery.model.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import co.com.photogallery.commons.Constants;
import co.com.photogallery.commons.dto.PermissionDTO;
import co.com.photogallery.commons.emuns.PermissionEnum;
import co.com.photogallery.commons.emuns.ResponseEnum;
import co.com.photogallery.commons.emuns.SeverityEnum;
import co.com.photogallery.commons.exception.BusinessException;
import co.com.photogallery.model.dao.IShareDAO;

@Service
public class ShareDAO implements IShareDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	/**
	 * Metodo encargado de insertar un permiso de un album sobre un usuario
	 * 
	 * @param idUser
	 * @param idAlbum
	 * @param permission
	 * @return
	 * @throws BusinessException
	 */
	public int insertShareAlbumUser(int idUser, int idAlbum, int permission) {
		return jdbcTemplate.update(Constants.INSERT_SHARE_ALBUM_USER, new Object[] { idAlbum, permission, idUser });
	}

	/**
	 * Metodo encargado de buscar si existe permisos sobre un usuario o un album
	 * 
	 * @param idUser
	 * @param idAlbum
	 * @return
	 * @throws BusinessException
	 */
	public List<PermissionDTO> getShareByUserOrAlbum(int idUser, int idAlbum) throws BusinessException {
		List<PermissionDTO> result = null;
		int id = 0;
		try {
			StringBuilder query1 = new StringBuilder().append("SELECT * FROM GALLERYDB.SHARE ");
			if (idUser != 0) {
				query1.append("WHERE ID_USER = ?");
				id = idUser;
			} else {
				query1.append("WHERE ID_ALBUM = ?");
				id = idAlbum;
			}

			result = jdbcTemplate.query(query1.toString(), new Object[] { id }, new RowMapper<PermissionDTO>() {
				public PermissionDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
					PermissionDTO c = new PermissionDTO();
					c.setIdAlbum(rs.getInt(1));
					c.setPermission(PermissionEnum.fromPermissionById(rs.getString(2)).getName());
					c.setIdUser(rs.getInt(3));
					return c;
				}
			});
		} catch (EmptyResultDataAccessException e) {
			throw new BusinessException(ResponseEnum.ERROR_404.getCode(), ResponseEnum.ERROR_404.getDescription(),
					SeverityEnum.INFO.name());
		}
		return result;
	}

	/**
	 * Metodo encargado de actualizar un permiso sobre un album par aun usuario
	 * 
	 * @param idUser
	 * @param idAlbum
	 * @param permission
	 * @return
	 * @throws BusinessException
	 */
	public int updateShareAlbumUser(int idUser, int idAlbum, int permission) throws BusinessException {
		if (getShareAlbumAndUser(idUser, idAlbum) != null) {
			return jdbcTemplate.update(Constants.UPDATE_SHARE_ALBUM_USER, new Object[] { permission, idAlbum, idUser });
		} else {
			throw new BusinessException(ResponseEnum.ERROR_404.getCode(), ResponseEnum.ERROR_404.getDescription(),
					SeverityEnum.INFO.name());
		}
	}

	/**
	 * Metodo encargado de buscar si un album y un usuario tienen permisos asigados
	 * 
	 * @param idUser
	 * @param idAlbum
	 * @return
	 */
	public List<PermissionDTO> getShareAlbumAndUser(int idUser, int idAlbum) {
		List<PermissionDTO> result = null;
		try {
			result = jdbcTemplate.query(Constants.GET_SHARE_ALBUM_AND_USER, new Object[] { idUser, idAlbum },
					new RowMapper<PermissionDTO>() {
						public PermissionDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
							PermissionDTO c = new PermissionDTO();
							c.setIdAlbum(rs.getInt(1));
							c.setPermission(PermissionEnum.fromPermissionById(rs.getString(2)).getName());
							c.setIdUser(rs.getInt(3));
							return c;
						}
					});
		} catch (EmptyResultDataAccessException e) {
			result = null;
		}
		return result;
	}
}
