package co.com.photogallery.model.dao;

public interface IPermissionDAO {

	/**
	 * Metodo encargado de insertar los permisos a la base de datos
	 * 
	 * @param id
	 * @param name
	 * @return
	 */
	public int insertPermission(int id, String name);
}
