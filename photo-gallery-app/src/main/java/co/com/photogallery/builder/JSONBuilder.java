package co.com.photogallery.builder;

import org.springframework.http.HttpStatus;

import co.com.photogallery.commons.emuns.ResponseEnum;

public class JSONBuilder {

	/**
	 * Metodo encargado de obtener el HttpStatus por medio del codigo de error
	 * 
	 * @param errorCode
	 * @return
	 */
	public static HttpStatus toHttpError(ResponseEnum errorCode) {
		switch (errorCode) {
		case ERROR_502:
			return HttpStatus.INTERNAL_SERVER_ERROR;
		case ERROR_400:
			return HttpStatus.BAD_REQUEST;
		case ERROR_404:
			return HttpStatus.NOT_FOUND;
		case ERROR_417:
			return HttpStatus.BAD_REQUEST;
		case ERROR_401:
			return HttpStatus.UNAUTHORIZED;
		default:
			return HttpStatus.INTERNAL_SERVER_ERROR;
		}
	}
}
